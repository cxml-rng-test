<?xml version="1.0"?>
<!-- $Id: testSuite.rng,v 1.4 2001/09/19 23:41:24 Bear Exp $ -->
<grammar xmlns="http://relaxng.org/ns/structure/1.0" ns="">
	<!--
		target namespace is intentionally set to "" so as not to interfere with
		prefix/URI bindings
	-->
	
	<start>
		<ref name="testSuite"/>
	</start>
	
	<define name="testSuite">
		<!-- a test suite represents a set of tests -->
		<element name="testSuite">
			<ref name="header"/>
			<oneOrMore>
				<choice>
					<!-- testSuites can be nested -->
					<ref name="testSuite"/>
					<!-- a single test case -->
					<ref name="testCase"/>
					<!--
						resources are by itself a RELAX NG pattern, and it can be 
						referenced from other patterns through <externalRef/>
						the scope of the resources are global.
					-->
					<element name="resource">
						<attribute name="href">
							<data type="token" key="moduleName" />
						</attribute>
						<ref name="anyElement"/>
					</element>
				</choice>
			</oneOrMore>
		</element>
	</define>

	<define name="testCase">
		<element name="testCase">
			<ref name="header"/>
			
			<choice>
				<!-- invalid patterns -->
				<oneOrMore>
					<element name="invalidPattern">
						<ref name="anyElement"/>
					</element>
				</oneOrMore>
				
				<!-- a valid pattern and test cases -->
				<group>
					<element name="validPattern">
						<!-- the compatibility of this schema with various features -->
						<optional>
							<attribute name="annotation">
								<ref name="ok.ng"/>
							</attribute>
						</optional>
						<optional>
							<attribute name="ididref">
								<ref name="ok.ng"/>
							</attribute>
						</optional>
						<optional>
							<attribute name="defaultValue">
								<ref name="ok.ng"/>
							</attribute>
						</optional>
						
						<!-- pattern itself -->
						<externalRef href="relaxng.rng"/>
					</element>
					
					<oneOrMore>
						<choice>
							<!-- valid test cases -->
							<element name="valid">
								<!-- the soundness of this document -->
								<optional>
									<attribute name="ididref">
										<ref name="ok.ng"/>
									</attribute>
								</optional>
								<ref name="anyElement"/>
							</element>
							<!-- invalid test cases -->
							<element name="invalid">
								<ref name="anyElement"/>
							</element>
						</choice>
					</oneOrMore>
				</group>
			</choice>
		</element>
	</define>
	
	<define name="ok.ng">
		<choice>
			<value>ok</value>
			<value>ng</value>
		</choice>
	</define>
	
	
	<define name="any">
		<zeroOrMore>
			<choice>
				<ref name="anyElement"/>
				<attribute>
					<anyName/>
					<text/>
				</attribute>
				<text/>
			</choice>
		</zeroOrMore>
	</define>
	
	<define name="anyElement">
		<element>
			<anyName/>
			<ref name="any"/>
		</element>
	</define>
	
	
	<!-- header is a place holder for meta-information -->
	<define name="header">
		<optional>
			<element name="header">
				<!-- this schema does not check the contents of headers -->
				<zeroOrMore>
					<ref name="anyElement"/>
				</zeroOrMore>
			</element>
		</optional>
	</define>
	
</grammar>
