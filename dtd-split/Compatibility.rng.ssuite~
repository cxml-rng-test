<?xml version="1.0" encoding="UTF-8" standalone="yes" ?>
<!DOCTYPE testSuite [

	<!ENTITY rngURI "http://relaxng.org/ns/structure/1.0">
	<!ENTITY compLibURI "http://relaxng.org/ns/compatibility/datatypes/1.0">
	<!ENTITY xsdlibURI "http://www.w3.org/2001/XMLSchema-datatypes">
]>
<testSuite
		xmlns:a="http://relaxng.org/ns/compatibility/annotations/1.0"
		xmlns:rng="&rngURI;"
		xmlns:foo="foo"
		>
	
	<title>RELAX NG DTD Compatibility Test Suite</title>
	<author>Sun Microsystems</author>
	<email>kohsuke.kawaguchi@sun.com</email>
	<documentation>
		This test suite tests the conformance of RELAX NG DTD compatibility processors. It is copyrighted by Sun Microsystems.
		
		Small part of this test suite relies on the existance of an implementation of XML Schema Part 2. Those test cases are marked by &lt;useXMLSchemaDatatypes/&gt; element in its header.
		
		$Id: Compatibility.rng.ssuite,v 1.6 2001/10/11 19:51:41 Bear Exp $
	</documentation>
	
	<testSuite>
		<title>Annotation feature</title>
		<documentation>
			Tests the compatibility with the annotation feature.
		</documentation>
		
		<testSuite>
			<title>valid test cases</title>
			
			<testCase>
				<correct>
					<grammar xmlns="&rngURI;">
						<a:annotation>
							text annotation at the top of siblings
						</a:annotation>
						<start>
							<element name="dummy">
								<ref name="body"/>
							</element>
						</start>
						
						<define name="body">
							<foo:foo/>
							<foo:foo>
								<empty/>	<!-- dummy RELAX NG elements -->
							</foo:foo>
							<a:annotation>
								This is OK because there is no preceding RELAX NG elements.
							</a:annotation>
							
							<group>
								<a:annotation>
									multiple annotation
								</a:annotation>
								<foo:foo/>
								<a:annotation>
									multiple annotation for one pattern
								</a:annotation>
								
								<foo:foo>
									<!-- it should be OK to have annotation inside a foreign element. -->
									<a:annotation/>
								</foo:foo>
								
								<element>
									<name>abc</name>
									<a:annotation>
										this is OK because it is after the "name" element.
									</a:annotation>
									<empty/>
								</element>
								
								<element>
									<name>abc</name>
									<a:annotation>
										multiple annotation
									</a:annotation>
									<foo:foo/>
									<a:annotation>
										multiple annotation
									</a:annotation>
									<empty/>
								</element>
								
								<element name="dummy">
									<value>123</value>
									<a:annotation>
										this is another case where it's OK to have an annotation after something.
									</a:annotation>
								</element>
								
								<empty>
									<a:annotation foo:abc="http:xyz" xml:lang="ja">
										it's OK to have attributes from foreign namespaces
									</a:annotation>
								</empty>
							</group>
						</define>
					</grammar>
				</correct>
				<!-- there is no test instance to test the validity -->
			</testCase>
			
		</testSuite><!-- end of the valid annotation test suite -->
			
		<testSuite>
			<title>invalid test cases</title>
			<documentation>
				Schemas that violates the annotation compatibility.
				Note that they are actually valid. They are just not compatible with the annotation feature.
			</documentation>
			
			<testSuite>
				<documentation>
					these test cases violate the 1st clause
					"it does not have any child elements".
				</documentation>
				
				<testCase>
					<correct annotation="incompatible">
						<grammar xmlns="&rngURI;">
							
							<a:annotation>
								<a:annotation/>
							</a:annotation>
							
							<start>
								<notAllowed/>
							</start>
						</grammar>
					</correct>
				</testCase>
				
				<testCase>
					<correct annotation="incompatible">
						<grammar xmlns="&rngURI;">
							
							<a:annotation>
								<foo:foo/>
							</a:annotation>
							
							<start>
								<notAllowed/>
							</start>
						</grammar>
					</correct>
				</testCase>
			</testSuite>
			
			<testSuite>
				<documentation>
					these test cases violate the 2nd clause:
					"it does not have any attribute whose namespace URI is the empty string, the RELAX NG namespace URI or the compatibility annotations namespace URI"
				</documentation>
				
				<testCase>
					<correct annotation="incompatible">
						<grammar xmlns="&rngURI;">
							
							<a:annotation local="att">
								text
							</a:annotation>
							
							<start>
								<notAllowed/>
							</start>
						</grammar>
					</correct>
				</testCase>
				
				<testCase>
					<correct annotation="incompatible">
						<grammar xmlns="&rngURI;">
							
							<a:annotation a:local="att">
								text
							</a:annotation>
							
							<start>
								<notAllowed/>
							</start>
						</grammar>
					</correct>
				</testCase>
				
				<testCase>
					<correct annotation="incompatible">
						<grammar xmlns="&rngURI;">
							
							<a:annotation rng:href="att">
								text
							</a:annotation>
							
							<start>
								<notAllowed/>
							</start>
						</grammar>
					</correct>
				</testCase>
			</testSuite>
			
			<testSuite>
				<documentation>
					these test cases violate the 3rd clause:
					"if it has a preceding sibling element from the RELAX NG namespace, then the nearest such preceding sibling element is an element that does not allow child elements (i.e. value, param or name)"
				</documentation>
				
				<testCase>
					<correct annotation="incompatible">
						<group xmlns="&rngURI;">
							
							<notAllowed/>
							<a:annotation/>
						</group>
					</correct>
				</testCase>
				
				<testCase>
					<correct annotation="incompatible">
						<group xmlns="&rngURI;">
							
							<notAllowed/>
							<foo:foo/>
							<a:annotation/>
						</group>
					</correct>
				</testCase>
			</testSuite>
		</testSuite> <!-- end of the invalid annotation test suite -->
	</testSuite> <!-- end of the annotation feature test suite -->
	
	
	
	
	
	
	<testSuite>
		<title>ID/IDREF feature</title>
		<documentation>
			Tests the compatibility and the soundness with the id/idref feature.
		</documentation>
		
		<testSuite>
			<title>valid test cases</title>
			
			<testCase>
				<title>very simple use of ID/IDREF</title>
				
				<correct>
					<element name="root" xmlns="&rngURI;">
						<zeroOrMore datatypeLibrary="&compLibURI;">
							<element name="item">
								<optional>
									<attribute name="id">
										<data type="ID"/>
									</attribute>
								</optional>
								<optional>
									<attribute name="idref">
										<data type="IDREF"/>
									</attribute>
								</optional>
								<optional>
									<attribute name="idrefs">
										<data type="IDREFS"/>
									</attribute>
								</optional>
							</element>
						</zeroOrMore>
					</element>
				</correct>
				
				<valid ididref="notsound">
					<root>
						<item id="abc"/>
						<item id="abc"/>
					</root>
				</valid>
				
				<valid>
					<root>
						<item idref="abc"/>
						<item id="abc"/>
					</root>
				</valid>
				
				<valid>
					<root>
						<item idrefs="abc abc abc"/>
						<item id="abc"/>
					</root>
				</valid>
				
				<valid ididref="notsound">
					<root>
						<item idref="abc"/>
					</root>
				</valid>
			</testCase>

			<testCase>
				<title>the same test using XML Schema datatypes</title>
				<requires datatypeLibrary="&xsdlibURI;"/>
				
				<correct>
					<element name="root" xmlns="&rngURI;">
						<zeroOrMore datatypeLibrary="&xsdlibURI;">
							<element name="item">
								<optional>
									<attribute name="id">
										<data type="ID"/>
									</attribute>
								</optional>
								<optional>
									<attribute name="idref">
										<data type="IDREF"/>
									</attribute>
								</optional>
								<optional>
									<attribute name="idrefs">
										<data type="IDREFS"/>
									</attribute>
								</optional>
							</element>
						</zeroOrMore>
					</element>
				</correct>
				
				<valid ididref="notsound">
					<root>
						<item id="abc"/>
						<item id="abc"/>
					</root>
				</valid>
				
				<valid>
					<root>
						<item idref="abc"/>
						<item id="abc"/>
					</root>
				</valid>
				
				<valid>
					<root>
						<item idrefs="abc abc abc"/>
						<item id="abc"/>
					</root>
				</valid>
				
				<valid ididref="notsound">
					<root>
						<item idref="abc"/>
					</root>
				</valid>
			</testCase>
			
		</testSuite> <!-- end valid test cases -->
		
		
		<testSuite>
			<title>invalid test cases</title>
			<documentation>
				Tests the compatibility violations.
			</documentation>
			
			<testSuite>
				<title>clause 1 violation</title>
				<documentation>
					its parent is an attribute element
				</documentation>
				
				<testCase>
					<correct ididref="incompatible">
						<element name="abc" xmlns="&rngURI;">
							<data type="ID" datatypeLibrary="&compLibURI;"/>
						</element>
					</correct>
				</testCase>
				
				<testCase>
					<correct ididref="incompatible">
						<grammar xmlns="&rngURI;">
							<start>
								<element name="abc">
									<ref name="foo"/>
								</element>
							</start>
							
							<define name="foo">
								<data type="IDREF" datatypeLibrary="&compLibURI;"/>
							</define>
						</grammar>
					</correct>
				</testCase>
				
				<testCase>
					<title>IDREFS as an element content</title>
					<correct ididref="incompatible">
						<element name="abc" xmlns="&rngURI;">
							<data type="IDREFS" datatypeLibrary="&compLibURI;"/>
						</element>
					</correct>
				</testCase>
			</testSuite> <!-- end clause 1 violation -->
			
			
			
			<testSuite>
				<title>clause 2 violation</title>
				<documentation>
					The 2nd clause is "the first child of its attribute parent is a name element"
				</documentation>
				
				<testCase>
					<correct ididref="incompatible">
						<element name="abc" xmlns="&rngURI;">
							<zeroOrMore>
								<attribute>
									<anyName/>
									<data type="ID" datatypeLibrary="&compLibURI;" />
								</attribute>
							</zeroOrMore>
						</element>
					</correct>
				</testCase>
				
				<testCase>
					<correct ididref="incompatible">
						<element name="abc" xmlns="&rngURI;">
							<attribute>
								<choice>
									<name>abc</name>
									<name>def</name>
								</choice>
								<data type="IDREF" datatypeLibrary="&compLibURI;" />
							</attribute>
						</element>
					</correct>
				</testCase>
				
				<testCase>
					<correct ididref="incompatible">
						<grammar xmlns="&rngURI;">
							<start>
								<element name="abc">
									<zeroOrMore>
										<attribute>
											<nsName ns="http:xyz"/>
											<ref name="foo"/>
										</attribute>
									</zeroOrMore>
								</element>
							</start>
							
							<define name="foo">
								<ref name="bar"/>
							</define>
							
							<define name="bar">
								<data type="IDREFS" datatypeLibrary="&compLibURI;" />
							</define>
						</grammar>
					</correct>
				</testCase>
			</testSuite> <!-- end clause 2 violation -->
			
			
			
			<testSuite>
				<title>clause 3 violation</title>
				<documentation>
					The 3rd clause is "the first child of the element ancestor is a name element"
				</documentation>
				
				<testCase>
					<correct ididref="incompatible">
						<element xmlns="&rngURI;">
							<choice>
								<name>abc</name>
								<name>def</name>
							</choice>
							<attribute name="goo">
								<data type="ID" datatypeLibrary="&compLibURI;" />
							</attribute>
						</element>
					</correct>
				</testCase>
				
				<testCase>
					<correct ididref="incompatible">
						<grammar xmlns="&rngURI;">
							<start>
								<element xmlns="&rngURI;">
									<anyName/>
									<ref name="foo"/>
								</element>
							</start>
							
							<define name="foo">
								<attribute name="goo">
									<data type="IDREF" datatypeLibrary="&compLibURI;" />
								</attribute>
							</define>
						</grammar>
					</correct>
				</testCase>
				
				<testCase>
					<correct ididref="incompatible">
						<grammar xmlns="&rngURI;">
							<start>
								<element xmlns="&rngURI;">
									<nsName ns="http:xyz">
										<except>
											<name>joe</name>
										</except>
									</nsName>
									
									<empty/>
									<ref name="foo"/>
								</element>
							</start>
							
							<define name="foo">
								<attribute name="goo">
									<data type="IDREFS" datatypeLibrary="&compLibURI;" />
								</attribute>
							</define>
						</grammar>
					</correct>
				</testCase>
			</testSuite> <!-- end clause 3 violation -->
			
			
			
			
			
			<testSuite>
				<title>clause 4 violation</title>
				<documentation>
					The 4th clause is "any attribute element that competes with its parent attribute element has an data or value child specifying a datatype associated with the same ID-type"
				</documentation>
				
				<testCase>
					<correct ididref="incompatible">
						<!-- foo/@bar is competing -->
						<element name="root" xmlns="&rngURI;">
							<element name="foo">
								<attribute name="bar">
									<data type="ID" datatypeLibrary="&compLibURI;" />
								</attribute>
							</element>
							<element name="foo">
								<attribute name="bar">
									<value>abc</value>
								</attribute>
							</element>
						</element>
					</correct>
				</testCase>
				
				
				<testCase>
					<documentation>
						This example is ID compatible because after the simplification,
						the content model of "root" is &lt;notAllowed/&gt;
					</documentation>
					<correct>
						<element name="root" xmlns="&rngURI;">
							<element name="foo">
								<attribute name="bar">
									<data type="ID" datatypeLibrary="&compLibURI;" />
								</attribute>
							</element>
							<notAllowed/>	<!-- this effectively kills other two elements -->
							<element name="foo">
								<attribute name="bar">
									<value>abc</value>
								</attribute>
							</element>
						</element>
					</correct>
				</testCase>
				
				
				<testCase>
					<documentation>
						This example is ID compatible, although it is ambiguous.
					</documentation>
					<correct>
						<choice xmlns="&rngURI;">
							<element name="foo">
								<attribute name="bar">
									<data type="ID" datatypeLibrary="&compLibURI;" />
								</attribute>
								
								<value>123</value>
							</element>
							
							<element name="foo">
								<attribute name="bar">
									<data type="ID" datatypeLibrary="&compLibURI;" />
								</attribute>
								
								<value>456</value>
							</element>
						</choice>
					</correct>
				</testCase>
				
				
				<testCase>
					<documentation>
						This example is ID compatible, because one of @bar is
						unreachable.
					</documentation>
					<correct>
						<grammar xmlns="&rngURI;">
							<start>
								<element name="foo">
									<attribute name="bar">
										<data type="ID" datatypeLibrary="&compLibURI;" />
									</attribute>
								</element>
							</start>
							
							<define name="unused">
								<element name="foo">
									<attribute name="bar">
										<data type="string" datatypeLibrary=""/>
									</attribute>
								</element>
							</define>
						</grammar>
					</correct>
				</testCase>
				
				
				
				<testCase>
					<correct ididref="incompatible">
						<!-- foo/@bar is competing with foo/@* -->
						<element name="root" xmlns="&rngURI;">
							<element name="foo">
								<attribute name="bar">
									<data type="ID" datatypeLibrary="&compLibURI;" />
								</attribute>
							</element>
							<element name="foo">
								<zeroOrMore>
									<attribute>
										<anyName/>
										<value>abc</value>
									</attribute>
								</zeroOrMore>
							</element>
						</element>
					</correct>
				</testCase>
				
				
				
				<testCase>
					<correct ididref="incompatible">
						<!-- foo/@bar is competing with */@* -->
						<element name="root" xmlns="&rngURI;">
							<element name="foo">
								<attribute name="bar">
									<data type="ID" datatypeLibrary="&compLibURI;" />
								</attribute>
							</element>
							<element>
								<anyName/>
								<zeroOrMore>
									<attribute>
										<anyName/>
										<value>abc</value>
									</attribute>
								</zeroOrMore>
							</element>
						</element>
					</correct>
				</testCase>
				
				
				
				<testCase>
					<correct>
						<!-- foo/@bar is not competing, although this name class is complex -->
						<element name="root" xmlns="&rngURI;">
							<element name="foo">
								<attribute name="bar">
									<data type="ID" datatypeLibrary="&compLibURI;" />
								</attribute>
							</element>
							<element>
								<anyName>
									<except>
										<name>foo</name>
									</except>
								</anyName>
								<attribute name="bar">
									<value>abc</value>
								</attribute>
							</element>
						</element>
					</correct>
				</testCase>
			</testSuite> <!-- end clause 4 violation -->
		</testSuite> <!-- end invalid test cases -->
	</testSuite> <!-- end ID/IDREF feature test -->
	
	
	
	
	<testSuite>
		<title>Attribute default values feature</title>
		<documentation>
			Tests the compatibility with the
			attribute default values feature.
		</documentation>
		
		<testSuite>
			<title>valid test cases</title>
			
			<testCase>
				<correct>
					<element name="foo" xmlns="&rngURI;">
						<optional>
							<attribute name="bar" a:defaultValue="t512">
								<data type="ID" datatypeLibrary="&compLibURI;"/>
							</attribute>
						</optional>
					</element>
				</correct>
			</testCase>
			
			<testCase>
				<correct>
					<grammar xmlns="&rngURI;">
						<start>
							<element name="foo">
								<ref name="body"/>
							</element>
						</start>
						
						<define name="body">
							<!-- after the simplification, this corresponds to
								<optional><attribute/></optional> -->
							<interleave>
								<ref name="empty"/>
								<group>
									<ref name="empty"/>
									<choice>
										<ref name="empty"/>
										<attribute name="bar" a:defaultValue="t512">
											<data type="ID" datatypeLibrary="&compLibURI;"/>
										</attribute>
									</choice>
									<ref name="empty"/>
								</group>
								<ref name="empty"/>
							</interleave>
						</define>
						
						<define name="empty">
							<choice>
								<notAllowed/>
								<oneOrMore>
									<empty/>
								</oneOrMore>
							</choice>
						</define>
					</grammar>
				</correct>
			</testCase>
		</testSuite> <!-- end valid test -->
		
		
		
		<testSuite>
			<title>clause 1 violation</title>
			<documentation>
				The 1st clause is "its first child is a name element"
			</documentation>
			
			<testCase>
				<correct defaultValue="incompatible">
					<element name="foo" xmlns="&rngURI;">
						<zeroOrMore>
							<attribute a:defaultValue="xyz">
								<nsName ns=""/>
								<data type="string"/>
							</attribute>
						</zeroOrMore>
					</element>
				</correct>
			</testCase>
			
			<testCase>
				<correct defaultValue="incompatible">
					<element name="foo" xmlns="&rngURI;">
						<zeroOrMore>
							<attribute a:defaultValue="xyz">
								<anyName/>
								<data type="string"/>
							</attribute>
						</zeroOrMore>
					</element>
				</correct>
			</testCase>
		</testSuite> <!-- end clause 1 violation -->
		
		
		
		<testSuite>
			<title>clause 2 violation</title>
			<documentation>
				The 2nd clause is "the first child of the containing element element is a name element"
			</documentation>
			
			<testCase>
				<correct defaultValue="incompatible">
					<grammar xmlns="&rngURI;">
						<start>
							<element>
								<choice>
									<name>abc</name>
									<name>def</name>
								</choice>
								<ref name="att"/>
							</element>
						</start>
						
						<define name="att">
							<optional>
								<attribute name="abc" a:defaultValue="xyz" />
							</optional>
						</define>
					</grammar>
				</correct>
			</testCase>
			
			<testCase>
				<correct defaultValue="incompatible">
					<element xmlns="&rngURI;">
						<anyName/>
						<optional>
							<attribute name="abc" a:defaultValue="xyz" />
						</optional>
					</element>
				</correct>
			</testCase>
		</testSuite> <!-- end clause 2 violation -->
		
		
		
		<testSuite>
			<title>clause 3 violation</title>
			<documentation>
				The 3rd clause is "the value of the a:defaultValue attribute matches the pattern contained in the attribute element"
			</documentation>
			
			<testCase>
				<correct defaultValue="incompatible">
					<element name="foo" xmlns="&rngURI;">
						<optional>
							<!-- number is an invalid ID value -->
							<attribute name="bar" a:defaultValue="123 456">
								<data type="ID" datatypeLibrary="&compLibURI;"/>
							</attribute>
						</optional>
					</element>
				</correct>
			</testCase>
			
			<testCase>
				<!--
					this one is actually compatible because
					<attribute> will be removed during the simplification.
				-->
				<correct>
					<grammar xmlns="&rngURI;">
						<start>
							<element name="foo">
								<optional>
									<attribute name="baz" a:defaultValue="551">
										<ref name="p1"/>
									</attribute>
								</optional>
							</element>
						</start>
						
						<define name="p1">
							<ref name="p2"/>
						</define>
						
						<define name="p2">
							<choice>
								<data type="ID" datatypeLibrary="&compLibURI;"/>
								<notAllowed/>
							</choice>
						</define>
					</grammar>
				</correct>
			</testCase>
			
			
			<testCase>
				<correct defaultValue="incompatible">
					<grammar xmlns="&rngURI;">
						<start>
							<element name="foo">
								<optional>
									<attribute name="bar" a:defaultValue="246">
										<ref name="p1"/>
									</attribute>
								</optional>
							</element>
						</start>
						
						<define name="p1">
							<choice>
								<group>
									<data type="ID" datatypeLibrary="&compLibURI;"/>
									<ref name="notAllowed"/>
								</group>
							</choice>
						</define>
						
						<define name="notAllowed">
							<notAllowed/>
						</define>
					</grammar>
				</correct>
			</testCase>
			
		</testSuite> <!-- end clause 3 violation -->
		
		
		
		<testSuite>
			<title>clause 4 violation</title>
			<documentation>
				The 4th clause is "the pattern in the attribute element does not contain a data or value element with a context-dependent datatype".
				
				This test suite relies on XML Schema datatypes implementation.
			</documentation>
			
			
			<testCase>
				<requires datatypeLibrary="&xsdlibURI;"/>
				<correct defaultValue="incompatible">
					<element name="foo" xmlns="&rngURI;">
						<optional>
							<!-- although "xyz" is always a valid QName value, this is prohibited -->
							<attribute name="bar" a:defaultValue="xyz">
								<data type="QName" datatypeLibrary="&xsdlibURI;" />
							</attribute>
						</optional>
					</element>
				</correct>
			</testCase>
			
			
			<testCase>
				<requires datatypeLibrary="&xsdlibURI;"/>
				<correct defaultValue="incompatible">
					<element name="foo" xmlns="&rngURI;">
						<optional>
							<attribute name="bar" a:defaultValue="abc">
								<data type="ENTITY" datatypeLibrary="&xsdlibURI;" />
							</attribute>
						</optional>
					</element>
				</correct>
			</testCase>
		</testSuite> <!-- end clause 4 violation -->
		
		
		
		<testSuite>
			<title>clause 5 violation</title>
			<documentation>
				The 5th clause is "it does not have a oneOrMore ancestor"
			</documentation>
			
			<testCase>
				<correct defaultValue="incompatible">
					<element name="foo" xmlns="&rngURI;">
						<optional>
							<oneOrMore>
								<attribute name="bar" a:defaultValue="xyz" />
							</oneOrMore>
						</optional>
					</element>
				</correct>
			</testCase>
			
			<testCase>
				<correct defaultValue="incompatible">
					<element name="foo" xmlns="&rngURI;">
						<optional>
							<zeroOrMore>
								<attribute name="bar" a:defaultValue="xyz" />
							</zeroOrMore>
						</optional>
					</element>
				</correct>
			</testCase>
			
			<testCase>
				<!-- this is compatible because the attribute will be removed during the simplification -->
				<correct>
					<element name="foo" xmlns="&rngURI;">
						<optional>
							<zeroOrMore>
								<attribute name="bar" a:defaultValue="cnn">
									<notAllowed/>
								</attribute>
							</zeroOrMore>
						</optional>
					</element>
				</correct>
			</testCase>
			
			<testCase>
				<!-- this is compatible because the attribute will be removed during the simplification -->
				<correct>
					<element name="foo" xmlns="&rngURI;">
						<optional>
							<zeroOrMore>
								<attribute name="bar" a:defaultValue="bbc" />
							</zeroOrMore>
						</optional>
						<notAllowed/>
					</element>
				</correct>
			</testCase>
			
			
			<testCase>
				<correct>
					<element name="foo" xmlns="&rngURI;">
						<oneOrMore><!-- this oneOrMore is OK -->
							<element name="xyz">
								<optional>
									<attribute name="bar" a:defaultValue="xyz" />
								</optional>
							</element>
						</oneOrMore>
					</element>
				</correct>
			</testCase>
		</testSuite> <!-- end clause 5 violation -->
		
		
		
		<testSuite>
			<title>clause 6 violation</title>
			<documentation>
				The 6th clause is "any ancestor that is a choice element has one child that is an empty element"
			</documentation>
			
			<testCase>
				<correct defaultValue="incompatible">
					<grammar xmlns="&rngURI;">
						<start>
							<element name="root">
								<!-- choice with non-empty child -->
								<choice>
									<element name="child">
										<empty/>
									</element>
									<optional>
										<attribute name="abc" a:defaultValue="abc"/>
									</optional>
								</choice>
							</element>
						</start>
					</grammar>
				</correct>
			</testCase>
			
			<testCase>
				<correct defaultValue="incompatible">
					<grammar xmlns="&rngURI;">
						<start>
							<element name="root">
								<!-- a choice with an element child. This is no good. -->
								<choice>
									<empty/>
									<element name="child">
										<empty/>
									</element>
									<attribute name="abc" a:defaultValue="abc"/>
								</choice>
							</element>
						</start>
					</grammar>
				</correct>
			</testCase>
			
			
			<testCase>
				<correct>
					<grammar xmlns="&rngURI;">
						<start>
							<element name="root">
								<!-- one of the branch is empty. So this is OK -->
								<choice>
									<ref name="empty"/>
									<attribute name="abc" a:defaultValue="abc"/>
								</choice>
							</element>
						</start>
						
						<define name="empty">
							<choice>
								<notAllowed/>
								<group>
									<oneOrMore>
										<empty/>
									</oneOrMore>
								</group>
								<notAllowed/>
							</choice>
						</define>
					</grammar>
				</correct>
			</testCase>
			
			
			<testCase>
				<correct>
					<grammar xmlns="&rngURI;">
						<start>
							<element name="root">
								<!-- it is OK to have sequence -->
								<optional>
									<attribute name="abc" a:defaultValue="abc"/>
									<attribute name="def" a:defaultValue="abc"/>
								</optional>
							</element>
						</start>
					</grammar>
				</correct>
			</testCase>
		</testSuite> <!-- end clause 6 violation -->
		
		
		
		<testSuite>
			<title>clause 7 violation</title>
			<documentation>
				The 7th clause is "it has at least one choice ancestor"
			</documentation>
			
			<testCase>
				<correct defaultValue="incompatible">
					<grammar xmlns="&rngURI;">
						<start>
							<element name="root">
								<attribute name="abc" a:defaultValue="abc"/>
							</element>
						</start>
					</grammar>
				</correct>
			</testCase>
			
			<testCase>
				<correct defaultValue="incompatible">
					<grammar xmlns="&rngURI;">
						<start>
							<element name="root">
								<choice>
									<attribute name="abc" a:defaultValue="abc"/>
									<ref name="notAllowed"/>
								</choice>
							</element>
						</start>
						
						<define name="notAllowed">
							<interleave>
								<empty/>
								<mixed>
									<list>
										<group>
											<empty/>
											<zeroOrMore>
												<notAllowed/>
											</zeroOrMore>
										</group>
									</list>
								</mixed>
							</interleave>
						</define>
					</grammar>
				</correct>
			</testCase>
		</testSuite> <!-- end clause 6 violation -->
		
		
		
		
		
		
		
		<testSuite>
			<title>clause 8 violation</title>
			<documentation>
				The 8th clause is about competing attributes.
			</documentation>
			
			<testCase>
				<correct defaultValue="incompatible">
					<grammar xmlns="&rngURI;">
						<start>
							<element name="root">
								<optional>
									<attribute name="abc" a:defaultValue="abc"/>
								</optional>
								<optional>
									<element name="root"><!-- competing element -->
										<empty/><!-- without attribute value -->
									</element>
								</optional>
							</element>
						</start>
					</grammar>
				</correct>
			</testCase>
			
			<testCase>
				<correct defaultValue="incompatible">
					<grammar xmlns="&rngURI;">
						<start>
							<element name="root">
								<optional>
									<attribute name="abc" a:defaultValue="abc"/>
								</optional>
								<optional>
									<element name="root"><!-- competing element -->
										<optional><!-- with a different default value -->
											<attribute name="abc" a:defaultValue="xyz"/>
										</optional>
									</element>
								</optional>
							</element>
						</start>
					</grammar>
				</correct>
			</testCase>
			
			<testCase>
				<requires datatypeLibrary="&xsdlibURI;"/>
				<correct defaultValue="incompatible">
					<grammar xmlns="&rngURI;" datatypeLibrary="&xsdlibURI;">
						<start>
							<element name="root">
								<optional>
									<attribute name="abc" a:defaultValue="5">
										<data type="integer"/>
									</attribute>
								</optional>
								<optional>
									<element name="root"><!-- competing element -->
										<optional>
											<attribute name="abc" a:defaultValue="+5">
												<!-- +5 and 5 are actually the same value in the value space, but the comparision must be done at the lexical level. -->
												<data type="integer"/>
											</attribute>
										</optional>
									</element>
								</optional>
							</element>
						</start>
					</grammar>
				</correct>
			</testCase>
			
			<testCase>
				<correct defaultValue="incompatible">
					<grammar xmlns="&rngURI;">
						<start>
							<element name="root">
								<optional>
									<attribute name="abc" a:defaultValue="abc"/>
								</optional>
								<optional>
									<element><!-- competing element -->
										<anyName/>
										<empty/>
									</element>
								</optional>
							</element>
						</start>
					</grammar>
				</correct>
			</testCase>
			
			<testCase>
				<correct>
					<grammar xmlns="&rngURI;">
						<start>
							<element name="root">
								<optional>
									<attribute name="abc" a:defaultValue="abc"/>
								</optional>
							</element>
						</start>
						
						<define name="unreferenced">
							<element name="root">
								<!-- this is not competing, because this pattern will be removed during the simplification -->
								<empty/>
							</element>
						</define>
					</grammar>
				</correct>
			</testCase>
			
			
			<testCase>
				<requires datatypeLibrary="&xsdlibURI;"/>
				<correct>
					<grammar xmlns="&rngURI;">
						<start>
							<element name="root">
								<optional>
									<attribute name="abc" a:defaultValue="3"/>
								</optional>
								<!-- these two have the same default value. So it's OK -->
								<element name="root">
									<optional>
										<attribute name="abc" a:defaultValue="3">
											<data type="integer" datatypeLibrary="&xsdlibURI;"/>
										</attribute>
									</optional>
								</element>
							</element>
						</start>
					</grammar>
				</correct>
			</testCase>
			
		</testSuite> <!-- end clause 7 violation -->
	</testSuite> <!-- end def att value feature test -->
	
</testSuite>